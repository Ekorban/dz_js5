/*
1.	Опишіть своїми словами, що таке метод об’єкту.
    Метод об'єкта - це функція, яка належить об'єкту.
2.	Який тип даних може мати значення властивості об’єкта?
    Об'єкт може містити будь-який тип даних, оскільки властивість є значенням або набором значень.
3.  Об'єкт це посилальний тип даних. Що означає це поняття?
    Посилальний тип – це особливий «посередницький» внутрішній тип,
    який використовується з метою передачі інформації  від крапки.
 */

function createNewUser() {
    let newUser = new Object ();
        newUser.userName = prompt("Enter you name");
        newUser.userSurname = prompt("Enter you Surname");
        newUser.getLogin = function (){
        return `${(newUser.userName + newUser.userSurname).toLowerCase()}`
    }
    return newUser;
}
let user = createNewUser();
console.log(user.getLogin());